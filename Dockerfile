FROM node:16-alpine as build

ENV NODE_OPTIONS='--max-old-space-size=4096'

WORKDIR /cache
COPY . /cache
COPY .env /

RUN npm install
RUN npm run build


FROM nginx:1.21-alpine

COPY --from=build /cache/build /usr/share/nginx/html

ADD ./docker/nginx.conf /etc/nginx/nginx.conf
ADD ./docker/frontend.conf /etc/nginx/conf.d/default.conf

RUN chown -R nginx:nginx /usr/share/nginx/html

EXPOSE 80

CMD /bin/sh -c "nginx"