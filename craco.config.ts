import path from 'path';
import * as dotenv from 'dotenv';

dotenv.config();

const CracoEnvPlugin = require('craco-plugin-env')

module.exports = {
    devServer: {
        port: 8000,
        proxy: {
            '/api': {
                target: process.env.REACT_APP_PROXY_PATH,
                changeOrigin: true, //false
            }
        },
    },
    webpack: {
        alias: {
            '@': path.resolve(__dirname, 'src')
        }
    },
    plugins: [
        {
            plugin: CracoEnvPlugin,
            options: {
                envDir: '/.env'
            }
        }
    ]
};