<h1>Hermes</h1>

<h2>Intro</h2>
<p>
The backend and frontend are separated on the project.
Since this repository is dedicated to the frontend, we will talk about it.
The project is made on a stack: <b>React/TypeScript/Chakra UI/TanStack Query/FSD 
Architecture/Docker/Gitlab Runner.</b>
</p>

<h2>Description</h2>
<p>
<b>Frontend</b> is used as a visual representation of books. <b>React and Chakra UI</b> 
for a beautiful wrapper. <br>
<b>TypeScript, TanStack Query and FSD Architecture</b> for convenient and safe development. 
<br>
<b>Docker и Gitlab Runner</b> for convenient automatic assembly.
</p>

<h2>Getting started</h2>
For a quick start, all you need is:

```docker compose up```

The ```env.example``` file is pretty clear in principle. Env file must be filled in completely. 
For example, where :
- REACT_APP_PROXY_PATH main domain ```google.com```
- REACT_APP_API_URL prefix to the API itself ```/api```
- REACT_APP_TOKEN_AUTH authorization token ```Token *************```