export const PROXY_PATH = process.env.REACT_APP_PROXY_PATH as string;
export const API_URL = process.env.REACT_APP_API_URL as string;
export const PATH_URL = `${PROXY_PATH}${API_URL}`
