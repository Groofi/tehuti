export const snakeToCamel = (to: string) => {
    return to.replace(/[^a-zA-Z0-9]+(.)/g, (m, chr) => chr.toUpperCase());
};

export const camelToSnake = (to: string) => {
    return to.replace(/[A-Z]/g, (chr) => `_${chr.toLowerCase()}`);
};

export const convertSnakeToCamelObjectKeys = (resObject: object) => {
    const newData: { [key: string]: string | object } = {};
    const data = Object.entries(resObject);

    data.forEach(([key, value]) => {
        const nameKey: string = snakeToCamel(key);

        if (typeof value === 'object') {
            if (value.length) {
                value = value.map((item: object) => convertSnakeToCamelObjectKeys(item)) as object | string;
            } else {
                value = convertSnakeToCamelObjectKeys(value) as object | string;
            }
        }
        newData[nameKey] = value as object | string;
    });

    return newData;
};