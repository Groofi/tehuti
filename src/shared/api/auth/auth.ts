import {useMutation} from '@tanstack/react-query';
import axios, {AxiosError} from 'axios';
import {IAuth, IToken} from '@/entities/auth';
import {PATH_URL} from '@/shared/constants';

export const useAuth = () => {
    return useMutation<IToken, AxiosError, IAuth>(data => {
            return axios.post(`${PATH_URL}token/`, data).then(data => {
                return data.data
            })
        }
    );
}