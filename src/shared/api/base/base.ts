import axios from 'axios';
import {QueryKey} from '@tanstack/react-query';
import {API_URL, PROXY_PATH} from '@/shared/constants';

export const queryBase = async ({queryKey}: { queryKey: QueryKey }) => {
    const url = `${PROXY_PATH}${API_URL}${queryKey[0] as string}`;
    const token = sessionStorage.getItem('token');
    const {data}: { data: object } = await axios.get(url, {headers: {Authorization: `Bearer ${token}`}});

    return data;
};