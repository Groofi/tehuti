import {useQuery} from '@tanstack/react-query';
import {AxiosError} from 'axios';
import {IBooks} from '@/entities/books';

interface IQueryBooks {
    count: number,
    next: string,
    previous: string,
    results: IBooks[],
}


export const useBooks = () => {
    return useQuery<undefined, AxiosError, IQueryBooks>({
            queryKey: ['books/'],
        }
    );
};