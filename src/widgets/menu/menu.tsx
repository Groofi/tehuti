import React, {FC} from 'react';
import {Box, Link} from '@chakra-ui/react';
import styles from './menu.module.scss';
import {SunIcon} from '@chakra-ui/icons';
import {GiExitDoor} from 'react-icons/all';

export const Menu: FC<{ setToken: (token: string) => void }> = ({setToken}) => {
    return (
        <Box backgroundColor={'#9a7952'} height={'70px'} display={'flex'} alignItems={'center'}>
            <Box className={styles.wrapper}>
                <Box className={styles.main}>
                    <Box>
                        <Link href={'/'}>
                            <Box className={styles.sun}>
                                <SunIcon color={'#D4B128'} w={12} h={12}/>
                            </Box>
                        </Link>
                    </Box>
                    <Box className={styles.box}>
                        <Link className={styles.link} color={'#D4B128'} href={'http://hermes.so-books.ru/admin'}>
                            Админка
                        </Link>
                        <Link className={styles.link} color={'#D4B128'} href={'http://hermes.so-books.ru/api'}>
                            Апи
                        </Link>
                        <Link className={styles.link} color={'#D4B128'} href={'https://gitlab.com/Groofi'}>
                            Гит
                        </Link>
                        <Link className={styles.link} color={'#D4B128'} display={'flex'} alignItems={'center'} href={'/'} onClick={() => {
                            sessionStorage.clear();
                            setToken('');
                        }}>
                            <GiExitDoor size={30} />
                        </Link>
                    </Box>
                </Box>
            </Box>
        </Box>
    )
};

