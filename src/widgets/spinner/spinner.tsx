import React, {FC} from 'react';
import {Spinner as BasicSpinner} from '@chakra-ui/spinner';
import {Wrapper} from '@/widgets/wrapper';

export const Spinner: FC = () => {
    return (
        <Wrapper>
            <BasicSpinner display={'flex'} textAlign={'center'} size='xl' speed='0.5s'/>
        </Wrapper>
    )
};

