import React, {FC, ReactNode} from 'react';
import {Box} from '@chakra-ui/react';
import styles from './wrapper.module.scss';

export const Wrapper: FC<{ children: ReactNode }> = ({children}) => {
    return (
        <Box className={styles.wrapper}>
            {children}
        </Box>
    )
};

