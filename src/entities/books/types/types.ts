export interface IBooks {
    id: number,
    author: string,
    made_in: string,
    box: string,
    name: string,
    year: number,
}

export interface IBooksForm {
    name: string,
    setName: (search: string) => void,
    nameBox: string,
    setNameBox: (search: string) => void
}