import React, {FC} from 'react';
import {Card, CardBody, CardFooter, CardHeader, Divider, Heading, Text, Image} from '@chakra-ui/react';
import {IBooks} from '../../types';
import BookIMG from '../../../../shared/assests/default-book.png'

export const BooksCard: FC<IBooks> = (
    {
        id,
        name,
        author,
        made_in,
        year,
        box
    }
) => {
    const is_staff = sessionStorage.getItem('is_staff')
    return (
        <Card key={id} sx={{backgroundColor: '#cfc4a8'}}>
            <CardHeader textAlign={'center'}>
                <Image
                    height={'350px'}
                    src={BookIMG}
                    alt='Books'
                    borderRadius='lg'
                />
                <Heading paddingTop={'15px'} size='md'>{name}</Heading>
            </CardHeader>
            <CardBody padding={'10px 20px 20px'} textAlign={'left'}>
                <Text fontWeight={200}>Автор: {author}</Text>
                <Text fontWeight={200}>Производство: {made_in}</Text>
                <Text fontWeight={200}>Год: {year}</Text>
            </CardBody>
            <Divider/>
            {is_staff === 'true' && (
                <CardFooter>
                    <Text textAlign={'center'}>Коробка: {box}</Text>
                </CardFooter>
            )}

        </Card>
    );
};
