import React, {FC} from 'react';
import {Box, Input} from '@chakra-ui/react';
import {IBooksForm} from '@/entities/books/types/types';
import styles from './books-form.module.scss';

export const BooksForm: FC<IBooksForm> = (
    {
        name,
        setName,
        nameBox,
        setNameBox
    }
) => {

    return (
        <Box className={styles.main}>
            <Input placeholder='Введите название книги' size='lg' value={name} backgroundColor={'white'} className={styles.input}
                   onChange={(event) => setName(event.target.value)}/>
            <Input placeholder='Введите название коробки' size='lg' value={nameBox} backgroundColor={'white'} className={styles.input}
                   onChange={(event) => setNameBox(event.target.value)}/>
        </Box>
    );
};
