export interface IAuth {
    username?: string,
    password?: string
}

export interface IToken {
    access: string,
    refresh: string,
    username: string,
    is_active: boolean,
    is_staff: boolean,
    is_superuser: boolean
}


