import * as React from "react"
import {ChakraProvider, Box} from "@chakra-ui/react";
import {Main, QueryClientProvider} from '@/app';
import styles from './App.module.scss'
import {extendTheme} from "@chakra-ui/react";
import type {StyleFunctionProps} from "@chakra-ui/styled-system";

const theme = extendTheme({
    styles: {
        global: (props: StyleFunctionProps) => ({
            body: {
                bg: 'rgb(193, 168, 138)',
            }
        })
    },
})

export const App = () => (
    <ChakraProvider theme={theme}>
        <QueryClientProvider>
            <Box className={styles.main} fontSize="xl">
                <Main/>
            </Box>
        </QueryClientProvider>
    </ChakraProvider>
)
