import React, {FC, useState} from 'react';
import {Menu} from '@/widgets';
import {Auth, Books} from '@/pages';

export const Main: FC = () => {
    const [token, setToken] = useState(sessionStorage.getItem('token'));

    if (!token) {
        return <Auth setToken={setToken}/>
    }

    return (
        <>
            <Menu setToken={setToken}/>
            <Books/>
        </>
    );
};
