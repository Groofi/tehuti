import React, {FC, ReactNode} from 'react';
import {QueryCache, QueryClient, QueryClientProvider as QCP} from '@tanstack/react-query';
import {queryBase} from '@/shared/api';
import axios from 'axios';
import {PATH_URL} from '@/shared/constants';

export const QueryClientProvider: FC<{ children: ReactNode }> = ({children}) => {
    const queryClient = new QueryClient(
        {
            queryCache: new QueryCache({
                onError: async (error: any, query) => {

                    if (error.response?.status === 401) {
                        const refresh = sessionStorage.getItem('refresh')
                        const item = await axios.post(`${PATH_URL}token/refresh/`, {'refresh': refresh})
                            .then(result => result).catch(error => {
                                sessionStorage.clear();
                                window.location.reload()
                            });

                        if (item) {
                            await sessionStorage.setItem('token', item?.data.access);
                            await queryClient.refetchQueries(query.queryKey);
                        }

                    }
                },
            }),
            defaultOptions: {
                queries: {
                    refetchOnWindowFocus: false,
                    queryFn: queryBase,
                    retry: 2
                },
            },
        }
    );

    return <QCP client={queryClient}>{children}</QCP>;
};