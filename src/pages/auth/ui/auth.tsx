import React, {FC, useEffect, useState} from 'react';
import styles from './auth.module.scss';
import {Box, Button, FormControl, FormErrorMessage, FormLabel, Input, Text, useToast} from '@chakra-ui/react';
import {Wrapper} from '@/widgets/wrapper';
import {Form, Formik} from 'formik';
import {useAuth} from '@/shared/api';
import {IAuth, IToken} from '@/entities/auth';
import jwt_decode from "jwt-decode";


export const Auth: FC<{ setToken: (token: string) => void }> = ({setToken}) => {
    const toast = useToast()
    const [errorMain, setErrorMain] = useState('')

    const {data, isLoading, isSuccess, error, mutate} = useAuth()

    useEffect(() => {
        if (data?.access) {
            toast({
                title: 'Добро пожаловать.',
                description: "Вы авторизовались на сайте so-books.ru.",
                status: 'info',
                position: 'bottom-right',
                duration: 5000,
                isClosable: true,
            })
            sessionStorage.setItem('token', data.access)
            sessionStorage.setItem('refresh', data.refresh)
            const decoded: IToken = jwt_decode(data.access);
            sessionStorage.setItem('is_active', String(decoded.is_active))
            sessionStorage.setItem('is_staff', String(decoded.is_staff))
            setToken(data.access as string)

        }
    }, [isSuccess])

    useEffect(() => {
        if (error?.message) {
            setErrorMain('Неправильный логин или пароль')
        }
    }, [error, errorMain])

    const initialValue = {
        username: '',
        password: ''
    }

    const validateField = (value: IAuth) => {
        const errors: IAuth = {}
        if (!value.username) {
            errors.username = 'Логин обязательное поле.'
        }
        if (!value.password) {
            errors.password = 'Пароль обязательное поле.'
        }
        return errors
    }

    return (
        <Wrapper>
            <Box display={'flex'} flexDirection={'column'} alignItems={'center'}>
                <Text paddingBottom={'20px'} color={'#945a13'}>Авторизация</Text>
                <Formik
                    initialValues={initialValue}
                    validate={values => validateField(values)}
                    onSubmit={(values, actions) => {
                        mutate(values)
                    }}
                >
                    {({
                          values,
                          errors,
                          touched,
                          handleChange,
                          handleSubmit,
                      }) => (
                        <Form className={styles.form} onSubmit={handleSubmit}>
                            <FormControl isInvalid={!!(errors.username && touched.username)}>
                                <FormLabel>Логин</FormLabel>
                                <Input name='username' type={'text'} value={values.username} onChange={handleChange}
                                       placeholder='Логин' backgroundColor={'white'}/>
                                <FormErrorMessage>{errors.username && errors.username}</FormErrorMessage>
                            </FormControl>
                            <FormControl isInvalid={!!(errors.password && touched.password)}>
                                <FormLabel>Пароль</FormLabel>
                                <Input name='password' type={'password'} value={values.password} onChange={handleChange}
                                       placeholder='Пароль' backgroundColor={'white'}/>
                                <FormErrorMessage>{errors.password && errors.password}</FormErrorMessage>
                            </FormControl>
                            <Text fontSize={'14'} color={'red'}>{errorMain}</Text>
                            <Button
                                alignItems={'center'}
                                mt={4}
                                colorScheme='teal'
                                backgroundColor={'#9a7952'}
                                isLoading={isLoading}
                                type='submit'
                            >
                                Войти
                            </Button>
                        </Form>
                    )}
                </Formik>
            </Box>
        </Wrapper>
    );
};
