import React, {useEffect, useState} from 'react';
import {Box, SimpleGrid, Spinner, Text} from '@chakra-ui/react';
import styles from './books.module.scss';
import {BooksCard, IBooks} from '@/entities/books';
import {useBooks} from '@/shared/api';
import {BooksForm} from '@/entities/books/ui/books-form/books-form';

export const Books = () => {

    const {data, isLoading, isError} = useBooks();
    const preparedData = data && data.results || [];
    const [name, setName] = useState('')
    const [nameBox, setNameBox] = useState('')
    const [books, setBooks] = useState<IBooks[]>(preparedData)

    useEffect(() => {
        setBooks(preparedData);
    }, [data])

    useEffect(() => {
        const filterBooks = preparedData.filter(book => {
            return book.name.includes(name);
        }).filter(bookBox => {
            return bookBox.box.includes(nameBox)
        })
        name || nameBox ? setBooks(filterBooks) : setBooks(preparedData);
    }, [name, nameBox])

    return (

        <Box className={styles.wrapper}>

            {!isLoading ? (
                <>
                    {!isError ? (
                        <>
                            <BooksForm name={name} setName={setName} nameBox={nameBox} setNameBox={setNameBox}/>
                            <SimpleGrid columns={[1, 2, 3]} spacing='40px'>
                                {books?.map((item) => {
                                    return (
                                        <BooksCard {...item}/>
                                    )
                                })}
                            </SimpleGrid>
                        </>
                    ) : (
                        <>
                            <Text textAlign={'center'} paddingTop={'100px'}>Ошибка запроса</Text>
                        </>
                    )}
                </>
            ) : (
                <Box className={styles.spin}>
                    <Spinner display={'flex'} textAlign={'center'} size='xl' speed='0.5s'/>
                </Box>
            )}
        </Box>
    );
};